class UsersController < ApplicationController
  def new
    @user = User.new
  end
  
  def create
    @user = User.new(require_params(params))
    if @user.save
      session[:user_id] = @user.id
      redirect_to root_url, notice: "Thank you for sign up"
    else
      render 'new'
    end
  end
  def show
    print session.keys
    print session.values
    @user = User.find(params[:id])
    
  end
  def require_params(params)
    params.require(:user).permit(:email, :password, :password_confirmation)
  end
end
